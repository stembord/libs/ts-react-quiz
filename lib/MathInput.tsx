import * as React from "react";
import { InlineMath } from "react-katex";
import { Input, InputGroup, InputGroupAddon, InputProps } from "reactstrap";

export interface IMathInputProps extends InputProps {}

export interface IMathInputState {}

const ROOT_STYLE: React.CSSProperties = {
  width: "auto",
  display: "inline-block"
};

export class MathInput extends React.PureComponent<
  IMathInputProps,
  IMathInputState
> {
  render() {
    return (
      <InputGroup style={ROOT_STYLE}>
        <Input {...this.props} />
        {this.props.value && (
          <InputGroupAddon addonType="append">
            <InlineMath math={this.props.value as any} />
          </InputGroupAddon>
        )}
      </InputGroup>
    );
  }
}
