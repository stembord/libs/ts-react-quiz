import * as React from "react";

export interface IExplanationProps {
  multipleSteps?: boolean;
  children: React.ReactChild | React.ReactChild[];
}

export class Explanation extends React.PureComponent<IExplanationProps> {
  static is(value: any): value is Explanation {
    return React.isValidElement(value) && value.type === Explanation;
  }

  render() {
    return null;
  }
}
