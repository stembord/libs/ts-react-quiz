import { Check, default as Octicon, X } from "@primer/octicons-react";
import * as React from "react";
import { Button, Container } from "reactstrap";
import { InternalExplanation } from "./InternalExplanation";
import { InternalPrompt } from "./Prompt";
import { QuestionData } from "./QuizData";

export interface IInternalAnswerQuestionProps {
  question: QuestionData;
  input: any[];
  correct?: boolean;
  explained?: boolean;
  explainationStep: number;
  update(fn: (state: any[]) => any[]): void;
  onExplain(): void;
  onSubmit(score: number): void;
  onNext(): void;
  onNextStep(): void;
}

export class InternalAnswerQuestion extends React.Component<
  IInternalAnswerQuestionProps
> {
  onSubmit = () => {
    this.props.onSubmit(this.props.question.prompt.getScore(this.props.input));
  };
  render() {
    return (
      <Container className="Answer Question clearfix" fluid>
        <InternalPrompt
          prompt={this.props.question.prompt}
          input={this.props.input}
          correct={this.props.correct}
          explained={this.props.explained}
          disabled={this.props.correct != null || this.props.explained != null}
          update={this.props.update}
        />
        <div className="clearfix mt-4">
          <div className="Buttons text-center">
            {this.props.correct === true && (
              <span className="mr-2">
                <Octicon icon={Check} size={24} />
              </span>
            )}
            {this.props.correct === false && (
              <span className="mr-2">
                <Octicon icon={X} size={24} />
              </span>
            )}
            {this.props.explained === true && (
              <p className="Explained mr-2 d-inline">Explained</p>
            )}
            <Button
              className="Explain mr-2"
              variant="secondary"
              disabled={this.props.explained === true}
              onClick={this.props.onExplain}
            >
              Show Explaination
            </Button>
            <Button
              className="Submit mr-2"
              disabled={
                this.props.correct != null || this.props.explained === true
              }
              onClick={this.onSubmit}
            >
              Submit
            </Button>
            {(this.props.correct != null || this.props.explained != null) && (
              <Button className="Next mr-2" onClick={this.props.onNext}>
                Next
              </Button>
            )}
          </div>
        </div>
        {this.props.explained && this.props.question.explaination ? (
          <div className="mt-4">
            <hr />
            <InternalExplanation
              explaination={this.props.question.explaination}
              step={this.props.explainationStep}
              onNext={this.props.onNextStep}
            />
          </div>
        ) : null}
      </Container>
    );
  }
}
