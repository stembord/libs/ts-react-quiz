import classNames from "classnames";
import * as React from "react";
import { Button, ButtonGroup } from "reactstrap";
import { QuestionData } from "./QuizData";

export interface IProgressProps {
  currentIndex: number;
  results: IResult[];
  setCurrent(index: number): void;
}

export class Progress extends React.PureComponent<IProgressProps> {
  render() {
    return (
      <ButtonGroup className="w-100 mb-4">
        {this.props.results.map((result, index) => {
          return (
            <Button
              key={index}
              disabled={this.props.currentIndex === index}
              className={classNames({
                "btn-primary": isDone(result),
                "btn-danger": isIncorrect(result),
                "btn-success": isCorrect(result),
                "btn-warning": isExplained(result)
              })}
              onClick={() => this.props.setCurrent(index)}
            >
              {index + 1}
            </Button>
          );
        })}
      </ButtonGroup>
    );
  }
}

import { IResult, isCorrect, isDone, isExplained, isIncorrect } from "./Quiz";
