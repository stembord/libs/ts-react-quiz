import * as React from "react";

export interface IChoiceProps {
  answer?: boolean;
  children: React.ReactChild;
}

export class Choice extends React.PureComponent<IChoiceProps> {
  static defaultProps = {
    answer: false
  };
  static is(value: any): value is Choice {
    return React.isValidElement(value) && value.type === Choice;
  }

  render() {
    return null;
  }
}
