import { Check, default as Octicon, X } from "@primer/octicons-react";
import * as React from "react";
import { Button, Container } from "reactstrap";
import { InternalExplanation } from "./InternalExplanation";
import { InternalPrompt } from "./Prompt";
import { QuestionData } from "./QuizData";

function noop() {}

export interface IInternalReviewQuestionProps {
  question: QuestionData;
  input: any[];
  correct?: boolean;
  explained?: boolean;
  score?: number;
  onNext(): void;
}

export class InternalReviewQuestion extends React.PureComponent<
  IInternalReviewQuestionProps
> {
  render() {
    return (
      <Container className="Review Question clearfix" fluid>
        <InternalPrompt
          prompt={this.props.question.prompt}
          input={this.props.input}
          correct={this.props.correct}
          explained={this.props.explained}
          disabled
          update={noop as any}
        />
        <div className="clearfix mt-4">
          <div className="Buttons text-center">
            {this.props.correct === true && (
              <span className="mr-2">
                <Octicon icon={Check} size={24} />
              </span>
            )}
            {this.props.correct === false && (
              <span className="mr-2">
                <Octicon icon={X} size={24} />
              </span>
            )}
            {this.props.explained === true && (
              <p className="Explained mr-2 d-inline">Explained</p>
            )}
            <Button className="Next mr-2" onClick={this.props.onNext}>
              Next
            </Button>
          </div>
        </div>
        {this.props.question.explaination && (
          <div className="mt-4">
            <hr />
            <InternalExplanation
              showAll
              explaination={this.props.question.explaination}
            />
          </div>
        )}
      </Container>
    );
  }
}
