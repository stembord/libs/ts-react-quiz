/// <reference path="./react-katex.d.ts" />

import * as Enzyme from "enzyme";
import * as EnzymeAdapter from "enzyme-adapter-react-16";
import { JSDOM } from "jsdom";
import * as React from "react";
import { InlineMath } from "react-katex";
import * as tape from "tape";
import {
  Choice,
  Description,
  Explanation,
  InputPrompt,
  InputType,
  MultipleChoicePrompt,
  Question,
  Quiz,
  TrueFalsePrompt
} from "./";

const dom = new JSDOM("<!doctype html><html><body></body></html>");

(global as any).document = dom.window.document;
(global as any).window = dom.window;

Enzyme.configure({ adapter: new EnzymeAdapter() });

tape("Quiz", (assert: tape.Test) => {
  const component = (
    <Quiz title="Test">
      <Description>
        <p>Each Question is very important!</p>
      </Description>
      <Description>
        <p>More information</p>
      </Description>
      <Question>
        <InputPrompt
          children={Input => (
            <>
              Fill in the{" "}
              {<Input type={InputType.String} answer={["blank", "Blank"]} />}
            </>
          )}
        />
        <Explanation>
          <p>It is a fill in the blank question that says fill in the _____</p>
        </Explanation>
      </Question>
      <Question>
        <InputPrompt
          children={Input => (
            <>
              <InlineMath math="1^2 + y^2 = x^2" />{" "}
              {
                <Input
                  type={InputType.Math}
                  answer="\sqrt(2)"
                  placeholder="x"
                />
              }
              {<Input type={InputType.Math} answer="1" placeholder="y" />}
            </>
          )}
        />
        <Explanation multipleSteps>
          <InlineMath math="x = \sqrt(2)" />
          <InlineMath math="y = 2" />
        </Explanation>
      </Question>
      <Question>
        <TrueFalsePrompt answer={true} true="Yes" false="No">
          <p>Is the square root of 2 a real number?</p>
        </TrueFalsePrompt>
        <Explanation>
          <p>Yes, google it</p>
        </Explanation>
      </Question>
      <Question>
        <MultipleChoicePrompt
          onlyOne
          randomOrder
          choices={[
            <Choice answer>
              <InlineMath math="\left(\prod _{i=1}^{n}x_{i}\right)^{\frac {1}{n}}={\sqrt[{n}]{x_{1}x_{2}\cdots x_{n}}}" />
            </Choice>,
            <Choice>
              <InlineMath math="\bar{x}= \frac{1}{n}\cdot \sum_{i=1}^{n} x_{i}" />
            </Choice>,
            <Choice>
              <InlineMath math="\frac{1}{\sqrt{2\cdot x}}" />
            </Choice>
          ]}
        >
          <p>
            Which of the following formulas gives the geometric mean of a series
            of numbers?
          </p>
        </MultipleChoicePrompt>
        <Explanation>
          <p>I do not want to explain this one</p>
        </Explanation>
      </Question>
    </Quiz>
  );
  assert.true(Quiz.parse(component.props));
  assert.end();
});
