declare module "react-katex" {
  import * as React from "react";

  export interface IBlockMathProps {
    math: string;
  }

  export function BlockMath(props: IBlockMathProps): JSX.Element;

  export interface IInlineMathProps {
    math: string;
  }

  export function InlineMath(props: IInlineMathProps): JSX.Element;
}
