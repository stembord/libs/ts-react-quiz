import * as React from "react";
import { Button } from "reactstrap";

export interface IInternalStartProps {
  count: number;
  title: React.ReactChild | React.ReactChild[];
  onStart(): void;
}

export class InternalStart extends React.PureComponent<IInternalStartProps> {
  render() {
    return (
      <div className="Start">
        <div className="text-center">
          <h5>{this.props.title}</h5>
          <p>
            <strong>{this.props.count} question(s) </strong>
            <Button onClick={this.props.onStart} className="ml-2">
              Start Quiz
            </Button>
          </p>
        </div>
      </div>
    );
  }
}
