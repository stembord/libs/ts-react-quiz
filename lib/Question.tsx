import * as React from "react";
import { IExplanationProps } from "./Explanation";
import { IInputPromptProps } from "./InputPrompt";
import { IMultipleChoicePromptProps } from "./MultipleChoicePrompt";
import { ITrueFalsePromptProps } from "./TrueFalsePrompt";

type IQuestionChild =
  | React.ReactElement<IInputPromptProps>
  | React.ReactElement<IMultipleChoicePromptProps>
  | React.ReactElement<ITrueFalsePromptProps>
  | React.ReactElement<IExplanationProps>;

export interface IQuestionProps {
  children: IQuestionChild | IQuestionChild[];
}

export class Question extends React.PureComponent<IQuestionProps> {
  static is(value: any): value is Question {
    return React.isValidElement(value) && value.type === Question;
  }

  render() {
    return null;
  }
}
