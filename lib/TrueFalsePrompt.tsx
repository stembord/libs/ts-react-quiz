import * as React from "react";
import { Input, InputGroup, Label } from "reactstrap";
import { IInternalPromptProps, IPromptProps } from "./Prompt";
import { TrueFalsePromptData } from "./QuizData";

export interface ITrueFalsePromptProps extends IPromptProps {
  answer: boolean;
  true: React.ReactChild;
  false: React.ReactChild;
}

export class TrueFalsePrompt extends React.PureComponent<
  ITrueFalsePromptProps
> {
  static is(value: any): value is TrueFalsePrompt {
    return React.isValidElement(value) && value.type === TrueFalsePrompt;
  }
}

export interface IInternalTrueFalsePromptProps extends IInternalPromptProps {
  prompt: TrueFalsePromptData;
  input: [boolean];
  update(fn: (input: [boolean]) => [boolean]): [boolean];
}

export class InternalTrueFalsePrompt extends React.PureComponent<
  IInternalTrueFalsePromptProps
> {
  onTrue = () => {
    this.props.update(() => [true]);
  };
  onFalse = () => {
    this.props.update(() => [false]);
  };
  render() {
    return (
      <div>
        <div>{this.props.prompt.prompt}</div>
        <div className="mb-2">
          <InputGroup
            check
            className="TrueFalse Input True mr-4"
            disabled={this.props.disabled}
          >
            <Label check>
              <Input
                type="radio"
                onChange={this.onTrue}
                checked={this.props.input[0] === true}
              />
              {this.props.prompt.true || "True"}
            </Label>
          </InputGroup>
          <InputGroup
            check
            className="TrueFalse Input False"
            disabled={this.props.disabled}
          >
            <Label>
              <Input
                type="radio"
                onChange={this.onFalse}
                checked={this.props.input[0] === false}
              />
              {this.props.prompt.false || "False"}
            </Label>
          </InputGroup>
        </div>
      </div>
    );
  }
}
