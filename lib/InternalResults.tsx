import * as React from "react";
import { Button, Container } from "reactstrap";

export interface IInternalResultsProps {
  correct: number;
  count: number;
  onReview(): void;
  onReset(): void;
}

export class InternalResults extends React.PureComponent<
  IInternalResultsProps
> {
  render() {
    return (
      <Container className="Results text-center" fluid>
        <Container className="Summary" fluid>
          <p>
            Correct: {this.props.correct} / {this.props.count}
          </p>
        </Container>
        <div className="Buttons">
          <Button className="Review mr-2" onClick={this.props.onReview}>
            Review
          </Button>
          <Button className="Reset" onClick={this.props.onReset}>
            Reset
          </Button>
        </div>
      </Container>
    );
  }
}
