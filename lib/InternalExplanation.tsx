import { default as Octicon, KebabHorizontal } from "@primer/octicons-react";
import { iter } from "@stembord/core";
import * as React from "react";
import { Button } from "reactstrap";
import { ExplainationData } from "./QuizData";

export interface IInternalExplanationProps {
  explaination: ExplainationData;
  showAll?: boolean;
  step?: number;
  onNext?(): void;
}

export class InternalExplanation extends React.Component<
  IInternalExplanationProps
> {
  static defaultProps: Partial<IInternalExplanationProps> = {
    showAll: false,
    step: 0,
    onNext: () => {}
  };

  getStep() {
    return (this.props.step as number) + 1;
  }

  render() {
    if (this.props.showAll) {
      return iter(this.props.explaination.children)
        .map((child, index) => <div key={index}>{child}</div>)
        .toArray();
    } else if (this.props.explaination.multipleSteps) {
      return (
        <>
          {iter(this.props.explaination.children)
            .take(this.getStep())
            .map((child, index) => <div key={index}>{child}</div>)
            .toArray()}
          {this.getStep() < this.props.explaination.children.length && (
            <div>
              <Button onClick={this.props.onNext}>
                <Octicon icon={KebabHorizontal} />
              </Button>
            </div>
          )}
        </>
      );
    } else {
      return this.props.explaination.children;
    }
  }
}
